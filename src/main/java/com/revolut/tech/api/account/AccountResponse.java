package com.revolut.tech.api.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.revolut.tech.core.model.Account;
import com.revolut.tech.core.model.Operation;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode
public final class AccountResponse {
    public final String name;
    public final String surname;
    public final String accountNumber;
    public final BigDecimal balance;
    public final List<Operation> operations;

    public AccountResponse(@JsonProperty("name") String name,
                           @JsonProperty("surname") String surname,
                           @JsonProperty("accountNumber") String accountNumber,
                           @JsonProperty("balance") BigDecimal balance,
                           @JsonProperty("operations") List<Operation> operations) {
        this.name = name;
        this.surname = surname;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.operations = ImmutableList.copyOf(operations);
    }

    public static AccountResponse from(Account account) {
        return new AccountResponse(
                account.accountHolder.name,
                account.accountHolder.surname,
                account.accountNumber,
                account.balance,
                account.operations);
    }
}
