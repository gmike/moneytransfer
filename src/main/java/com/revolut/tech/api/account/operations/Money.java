package com.revolut.tech.api.account.operations;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Currency;

public final class Money {

    @Digits(integer=100, fraction=2)
    @DecimalMin(value="0.01")
    public final BigDecimal amount;
    public final Currency currency;

    public Money(@JsonProperty("amount") BigDecimal amount,
                 @JsonProperty("currency") Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }
}
