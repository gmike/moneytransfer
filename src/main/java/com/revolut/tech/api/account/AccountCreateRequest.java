package com.revolut.tech.api.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

public final class AccountCreateRequest {

    @NotBlank
    public final String name;
    @NotBlank
    public final String surname;
    @NotBlank
    public final String countryCode;

    public AccountCreateRequest(@JsonProperty(value = "name")  String name,
                                @JsonProperty(value = "surname")  String surname,
                                @JsonProperty(value = "countryCode") String countryCode) {
        this.name = name;
        this.surname = surname;
        this.countryCode = countryCode;
    }
}
