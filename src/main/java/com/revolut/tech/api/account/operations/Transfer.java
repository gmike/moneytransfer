package com.revolut.tech.api.account.operations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;

@Immutable
public final class Transfer {

    @NotEmpty
    public final String destinationAccountNumber;
    @NotNull
    public final Money money;

    @JsonCreator
    public Transfer(@JsonProperty("destinationAccountNumber") String destinationAccountNumber,
                    @JsonProperty("money") Money money) {
        this.destinationAccountNumber = destinationAccountNumber;
        this.money = money;
    }
}
