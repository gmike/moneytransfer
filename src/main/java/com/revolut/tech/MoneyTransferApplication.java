package com.revolut.tech;

import com.revolut.tech.core.service.OperationsService;
import com.revolut.tech.repository.AccountsRepository;
import com.revolut.tech.resources.account.AccountResource;
import com.revolut.tech.resources.account.operations.AccountOperationsResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MoneyTransferApplication extends Application<MoneyTransferConfiguration> {

    private final AccountsRepository accountsRepository= new AccountsRepository();
    private final OperationsService operationsService = new OperationsService(accountsRepository);
    private final AccountResource accountResource = new AccountResource(accountsRepository);
    private final AccountOperationsResource operationsResource = new AccountOperationsResource(operationsService);

    public static void main(final String[] args) throws Exception {
        new MoneyTransferApplication().run(args);
    }

    @Override
    public String getName() {
        return "MoneyTransferApplication";
    }

    @Override
    public void initialize(final Bootstrap<MoneyTransferConfiguration> bootstrap) {
        // TODO: application initialization
    }


    @Override
    public void run(MoneyTransferConfiguration configuration,
                    Environment environment) {
        environment.jersey().register(accountResource);
        environment.jersey().register(operationsResource);
    }
}
