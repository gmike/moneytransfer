package com.revolut.tech.resources.account.operations;

import com.revolut.tech.api.account.operations.Money;
import com.revolut.tech.api.account.operations.Transfer;
import com.revolut.tech.core.service.OperationsService;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;
import javax.ws.rs.*;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/account")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequiredArgsConstructor
public class AccountOperationsResource {

    private final OperationsService operationsService;

    @PUT
    @Path("/{accountNumber}/operations/pay-in")
    public void payIn(@PathParam("accountNumber") String accountNumber, @Valid Money money) {
        operationsService.payIn(accountNumber, money);
    }

    @PUT
    @Path("/{accountNumber}/operations/transfer")
    public void transfer(@PathParam("accountNumber") String accountNumber, @Valid Transfer transfer) {
        operationsService.transfer(accountNumber, transfer.destinationAccountNumber, transfer.money);
    }
}
