package com.revolut.tech.resources.account;

import com.revolut.tech.api.account.AccountCreateRequest;
import com.revolut.tech.api.account.AccountResponse;
import com.revolut.tech.core.AccountFactory;
import com.revolut.tech.core.model.Account;
import com.revolut.tech.repository.AccountsRepository;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import javax.ws.rs.*;

import static com.revolut.tech.repository.AccountsRepository.ACCOUNT_NOT_FOUND_MESSAGE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/account")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@AllArgsConstructor
public class AccountResource {

    private final AccountsRepository accountsRepository;

    @POST
    public AccountResponse create(@Valid AccountCreateRequest accountCreateRequest) {
        Account account = AccountFactory.from(accountCreateRequest);
        accountsRepository.save(account);
        return AccountResponse.from(account);
    }

    @GET
    public AccountResponse getAccount(@QueryParam("accountNumber") String accountNumber) {
        Account account = accountsRepository
                .find(accountNumber)
                .orElseThrow(() -> new NotFoundException(String.format(ACCOUNT_NOT_FOUND_MESSAGE, accountNumber)));
        return AccountResponse.from(account);
    }
}
