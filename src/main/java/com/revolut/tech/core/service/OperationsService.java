package com.revolut.tech.core.service;

import com.revolut.tech.api.account.operations.Money;
import com.revolut.tech.core.model.Account;
import com.revolut.tech.core.model.Operation;
import com.revolut.tech.repository.AccountsRepository;
import lombok.RequiredArgsConstructor;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PathParam;
import java.math.BigDecimal;

import static com.revolut.tech.core.model.OperationType.PAY_IN;
import static com.revolut.tech.core.model.OperationType.TRANSFER;
import static com.revolut.tech.repository.AccountsRepository.ACCOUNT_NOT_FOUND_MESSAGE;
import static java.lang.String.format;

@RequiredArgsConstructor
public class OperationsService {

    private final AccountsRepository accountsRepository;

    public synchronized void payIn(String accountNumber, Money money) {
        Account account = findAccount(accountNumber);
        validateEqualCurrencies(account, money);
        Operation payIn = new Operation(PAY_IN, "", money.amount);
        accountsRepository.update(accountNumber, payIn);
    }

    public synchronized void transfer(String sourceAccountNumber, String destinationAccountNumber, Money money) {
        Account sourceAccount = findAccount(sourceAccountNumber);
        Account destinationAccount = findAccount(destinationAccountNumber);

        validateEqualCurrencies(sourceAccount, money);
        validateEqualCurrencies(destinationAccount, money);

        makeTransfer(sourceAccount, destinationAccount, money);
    }

    private void makeTransfer(Account sourceAccount, Account destinationAccount, Money money) {
        checkAvailableResources(sourceAccount, money.amount);

        Operation sourceAccountOperation = new Operation(
                TRANSFER, format("Payment to %s", destinationAccount.accountNumber), money.amount.negate());
        Operation destinationAccountOperation = new Operation(
                TRANSFER, format("Payment from %s", sourceAccount.accountNumber), money.amount);

        accountsRepository.update(sourceAccount.accountNumber, sourceAccountOperation);
        accountsRepository.update(destinationAccount.accountNumber, destinationAccountOperation);
    }

    private void checkAvailableResources(Account account, BigDecimal amount) {
        if(account.balance.compareTo(amount) <= 0) {
            throw new BadRequestException(format(
                    "Insufficient amount of money to make transfer (available=%s, needed=%s)", account.balance, amount));
        }
    }

    private Account findAccount(@PathParam("accountNumber") String accountNumber) {
        return accountsRepository.find(accountNumber)
                .orElseThrow(() -> new NotFoundException(format(ACCOUNT_NOT_FOUND_MESSAGE, accountNumber)));
    }

    private static void validateEqualCurrencies(Account account, Money money) {
        if (!account.currency.equals(money.currency)) {
            throw new IllegalArgumentException(format(
                    "Incorrect currency. The account accepts money in %s", account.currency));
        }
    }
}
