package com.revolut.tech.core;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Currency;

import static java.util.Currency.getInstance;

@RequiredArgsConstructor
public enum CountryCode {
    PL(getInstance("PLN")), UK(getInstance("GBP")), DE(getInstance("EUR"));

    @Getter
    private final Currency currency;
}
