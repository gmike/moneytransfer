package com.revolut.tech.core.model;

import com.google.common.collect.ImmutableList;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

public final class Account {

    public final String accountNumber;
    public final Currency currency;
    public final BigDecimal balance;
    public final AccountHolder accountHolder;
    public final List<Operation> operations;

    public Account(String accountNumber, Currency currency, BigDecimal balance,
                   AccountHolder accountHolder, List<Operation> operations) {
        this.accountNumber = accountNumber;
        this.currency = currency;
        this.balance = balance;
        this.accountHolder = accountHolder;
        this.operations = ImmutableList.copyOf(operations);
    }
}
