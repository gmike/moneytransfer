package com.revolut.tech.core.model;

public enum OperationType {
    PAY_IN, WITHDRAWAL, TRANSFER;
}
