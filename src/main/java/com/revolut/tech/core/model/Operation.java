package com.revolut.tech.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

@ToString
public class Operation {

    public final OperationType operationType;
    public final String content;
    public final BigDecimal amount;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public final LocalDateTime date = now();

    public Operation(@JsonProperty("operationType") OperationType operationType,
                     @JsonProperty("content") String content,
                     @JsonProperty("amount") BigDecimal amount) {
        this.operationType = operationType;
        this.content = content;
        this.amount = amount;
    }
}
