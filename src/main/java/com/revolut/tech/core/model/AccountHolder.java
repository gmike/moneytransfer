package com.revolut.tech.core.model;

public final class AccountHolder {
    public final String name;
    public final String surname;

    public AccountHolder(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
}
