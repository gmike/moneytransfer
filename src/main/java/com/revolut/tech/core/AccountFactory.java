package com.revolut.tech.core;

import com.revolut.tech.api.account.AccountCreateRequest;
import com.revolut.tech.core.model.Account;
import com.revolut.tech.core.model.AccountHolder;
import com.revolut.tech.core.model.Operation;

import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static java.util.Collections.emptyList;
import static org.iban4j.CountryCode.getByCode;
import static org.iban4j.Iban.random;

public class AccountFactory {

    public static Account from(AccountCreateRequest accountCreateRequest) {
        return new Account(
                accountNumber(accountCreateRequest.countryCode),
                CountryCode.valueOf(accountCreateRequest.countryCode).getCurrency(),
                ZERO,
                new AccountHolder(accountCreateRequest.name, accountCreateRequest.surname),
                emptyList());
    }

    public static Account from(Account account, Operation operation) {
        List<Operation> updatedOperations = new ArrayList<>(account.operations);
        updatedOperations.add(operation);
        return new Account(
                account.accountNumber,
                account.currency,
                account.balance.add(operation.amount),
                account.accountHolder,
                updatedOperations
        );
    }

    private static String accountNumber(String countryCode) {
        return random(getByCode(countryCode)).toString();
    }
}
