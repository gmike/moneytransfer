package com.revolut.tech.repository;

import com.revolut.tech.core.AccountFactory;
import com.revolut.tech.core.model.Account;
import com.revolut.tech.core.model.Operation;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class AccountsRepository {

    public static final String ACCOUNT_NOT_FOUND_MESSAGE = "Account with number %s does not exist";

    private Map<String, Account> accounts = new HashMap<>();

    public synchronized Account save(Account account) {
        accounts.put(account.accountNumber, account);
        return account;
    }

    public synchronized Optional<Account> find(String accountNumber) {
        return ofNullable(accounts.get(accountNumber));
    }

    public synchronized void update(String accountNumber, Operation operation) {
        find(accountNumber).ifPresent((account) -> save(AccountFactory.from(account, operation)));
    }
}
