package com.revolut.tech

import io.dropwizard.testing.ResourceHelpers
import io.dropwizard.testing.junit.DropwizardAppRule
import org.glassfish.jersey.client.JerseyClientBuilder
import org.junit.Rule
import spock.lang.Specification

import javax.ws.rs.client.Client

class SpecificationIT extends Specification {

    @Rule
    final DropwizardAppRule<MoneyTransferConfiguration> RULE =
            new DropwizardAppRule<MoneyTransferConfiguration>(MoneyTransferApplication.class,
                    ResourceHelpers.resourceFilePath("config.yml"));

    final Client client = new JerseyClientBuilder().build()
}
