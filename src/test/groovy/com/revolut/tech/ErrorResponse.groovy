package com.revolut.tech

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse {
    public final int code
    public final String message
    public final List<String> errors

    @JsonCreator
    public ErrorResponse(@JsonProperty('code') int code,
                         @JsonProperty('message') String message,
                         @JsonProperty("errors") List<String> errors) {
        this.code = code
        this.message = message
        this.errors = errors
    }
}
