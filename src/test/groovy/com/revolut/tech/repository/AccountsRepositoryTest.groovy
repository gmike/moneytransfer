package com.revolut.tech.repository

import com.revolut.tech.core.model.Account
import com.revolut.tech.core.model.AccountHolder
import com.revolut.tech.core.model.Operation
import spock.lang.Specification
import spock.lang.Subject

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.stream.IntStream

import static com.revolut.tech.core.model.OperationType.PAY_IN
import static java.math.BigDecimal.ZERO
import static java.util.Currency.getInstance
import static org.iban4j.Iban.random

class AccountsRepositoryTest extends Specification {

    private static final int THREADS_COUNT = 16

    private static final ACCOUNT_HOLDER = new AccountHolder('foo', 'bar')

    @Subject
    private AccountsRepository accountsRepository = new AccountsRepository()

    def 'should save account'() {
        given:
        String accountNumber = random().toString()
        Account account = new Account(accountNumber, getInstance("PLN"), ZERO, ACCOUNT_HOLDER, [])

        when:
        accountsRepository.save(account)

        then:
        accountsRepository.accounts.get(accountNumber) == account
    }

    def 'should find account in repository'() {
        given:
        String accountNumber = random().toString()
        Account expected = new Account(accountNumber, getInstance("PLN"), ZERO, ACCOUNT_HOLDER, [])
        accountsRepository.save(expected)

        when:
        Optional<Account> actual = accountsRepository.find(accountNumber)

        then:
        actual.isPresent()
        actual.get() == expected
    }

    def 'should handle many threads updating account'() {
        given:
        String accountNumber = random().toString()
        Account account = new Account(accountNumber, getInstance("PLN"), ZERO, ACCOUNT_HOLDER, [])
        accountsRepository.save(account);

        when:
        ExecutorService executor = Executors.newFixedThreadPool(THREADS_COUNT)
        IntStream.rangeClosed(1, THREADS_COUNT).forEach {
            executor.submit(new Runnable() {
                @Override
                void run() {
                    accountsRepository.update(accountNumber, new Operation(PAY_IN, '', it as BigDecimal))
                }
            })
        }
        executor.shutdown()
        executor.awaitTermination(5, TimeUnit.SECONDS)

        then:
        accountsRepository.find(accountNumber).get().operations.size() == THREADS_COUNT
    }
}
