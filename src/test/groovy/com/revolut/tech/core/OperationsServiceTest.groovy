package com.revolut.tech.core

import com.revolut.tech.api.account.operations.Money
import com.revolut.tech.core.model.Account
import com.revolut.tech.core.model.AccountHolder
import com.revolut.tech.core.service.OperationsService
import com.revolut.tech.repository.AccountsRepository
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import javax.ws.rs.BadRequestException
import javax.ws.rs.NotFoundException

import static com.revolut.tech.core.model.OperationType.PAY_IN
import static com.revolut.tech.core.model.OperationType.TRANSFER
import static java.util.Currency.getInstance
import static org.iban4j.CountryCode.PL
import static org.iban4j.Iban.random

class OperationsServiceTest extends Specification {

    private static final String ACCOUNT_NUMBER_A = random(PL).toString()
    private static final String ACCOUNT_NUMBER_B = random(PL).toString()
    private static final String NON_EXISTING_ACCOUNT_NUMBER = random(PL).toString()
    private static final String PLN = 'PLN'
    private static final BigDecimal INITIAL_BALANCE = 10
    private static final ACCOUNT_HOLDER = new AccountHolder('foo', 'bar')

    private final AccountsRepository accountsRepository = new AccountsRepository()
    @Subject
    private final OperationsService operationsService = new OperationsService(accountsRepository)

    def setup() {
        accountsRepository.save new Account(ACCOUNT_NUMBER_A, getInstance(PLN), INITIAL_BALANCE, ACCOUNT_HOLDER, [])
        accountsRepository.save new Account(ACCOUNT_NUMBER_B, getInstance(PLN), INITIAL_BALANCE, ACCOUNT_HOLDER, [])
    }

    def 'should throw IllegalArgumentException when money in incorrect currency payed in'() {
        when:
        operationsService.payIn(ACCOUNT_NUMBER_A, new Money(20, getInstance('GBP')))

        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Incorrect currency. The account accepts money in $PLN" as String
    }

    def 'should throw NotFoundException when account for pay in does not exist'() {
        when:
        operationsService.payIn(NON_EXISTING_ACCOUNT_NUMBER, new Money(20, getInstance(PLN)))

        then:
        NotFoundException ex = thrown()
        ex.message == "Account with number $NON_EXISTING_ACCOUNT_NUMBER does not exist" as String
    }

    def 'should record a pay in transaction'() {
        given:
        BigDecimal amount = 20.50

        when:
        operationsService.payIn(ACCOUNT_NUMBER_A, new Money(amount, getInstance(PLN)))

        then:
        Account account = accountsRepository.find(ACCOUNT_NUMBER_A).get()
        account.balance == INITIAL_BALANCE.plus(amount)
        account.operations.size() == 1
        account.operations[0].amount == amount
        account.operations[0].operationType == PAY_IN
    }

    def 'should throw IllegalArgumentException when money in incorrect currency transferred'() {
        when:
        operationsService.transfer(ACCOUNT_NUMBER_A, ACCOUNT_NUMBER_B, new Money(20, getInstance("GBP")))

        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Incorrect currency. The account accepts money in $PLN" as String
    }

    @Unroll
    def 'should throw NotFoundException when transfer #testCase'() {
        when:
        operationsService.transfer(sourceAccountNumber, destinationAccountNumner, new Money(20, getInstance("PLN")))

        then:
        NotFoundException ex = thrown()
        ex.message == "Account with number $NON_EXISTING_ACCOUNT_NUMBER does not exist" as String

        where:
        sourceAccountNumber         | destinationAccountNumner    | testCase
        ACCOUNT_NUMBER_A            | NON_EXISTING_ACCOUNT_NUMBER | 'source account does not exist'
        NON_EXISTING_ACCOUNT_NUMBER | ACCOUNT_NUMBER_A            | 'destination account does not exist'
    }

    def 'should throw BadRequestException when insufficient source account balance to make a transfer'() {
        when:
        operationsService.transfer(ACCOUNT_NUMBER_A, ACCOUNT_NUMBER_B, new Money(200, getInstance('PLN')))

        then:
        BadRequestException ex = thrown()
        ex.message == 'Insufficient amount of money to make transfer (available=10, needed=200)'
    }

    def 'should record transfer operation on source and destination accounts'() {
        given:
        BigDecimal amount = 9

        when:
        operationsService.transfer(ACCOUNT_NUMBER_A, ACCOUNT_NUMBER_B, new Money(amount, getInstance('PLN')))

        then:
        Account accountA = accountsRepository.find(ACCOUNT_NUMBER_A).get()
        accountA.balance == INITIAL_BALANCE.subtract(amount)
        accountA.operations.size() == 1
        accountA.operations[0].operationType == TRANSFER
        accountA.operations[0].amount == amount.negate()

        then:
        Account accountB = accountsRepository.find(ACCOUNT_NUMBER_B).get()
        accountB.balance == INITIAL_BALANCE.add(amount)
        accountB.operations.size() == 1
        accountB.operations[0].operationType == TRANSFER
        accountB.operations[0].amount == amount
    }
}
