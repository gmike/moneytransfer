package com.revolut.tech.resources.account.operations
import com.revolut.tech.ErrorResponse
import com.revolut.tech.SpecificationIT
import com.revolut.tech.api.account.AccountCreateRequest
import com.revolut.tech.api.account.AccountResponse
import com.revolut.tech.api.account.operations.Money
import com.revolut.tech.api.account.operations.Transfer

import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

import static com.revolut.tech.core.CountryCode.PL
import static com.revolut.tech.core.model.OperationType.PAY_IN
import static com.revolut.tech.core.model.OperationType.TRANSFER
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400
import static org.eclipse.jetty.http.HttpStatus.NO_CONTENT_204

class AccountOperationsResourceIT extends SpecificationIT {

    private static final String ACCOUNTS_PATH = 'http://localhost:8080/account'

    def 'should top up account with money payed in'() {
        given:
        String accountNumber = createAccount(new AccountCreateRequest('foo', 'bar', 'PL')).accountNumber
        BigDecimal amount = 20.11
        Money money = new Money(amount, PL.currency)

        when:
        Response response = payIn(accountNumber, money);

        then:
        response.status == NO_CONTENT_204
        AccountResponse accountResponse = getAccount(accountNumber)
        accountResponse.balance == amount
        accountResponse.operations.size() == 1
        accountResponse.operations[0].operationType == PAY_IN
        accountResponse.operations[0].amount == amount
    }

    def 'returns error when invalid amount payed in'() {
        given:
        String accountNumber = createAccount(new AccountCreateRequest('foo', 'bar', 'PL')).accountNumber
        Money money = new Money(-1, PL.currency)

        when:
        ErrorResponse errorResponse = client.target(payInPath(accountNumber))
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(money))
                .readEntity(ErrorResponse.class)

        then:
        errorResponse.errors == ['amount must be greater than or equal to 0.01']
    }

    def 'returns error when payed in amount has more than 2 fraction digits'() {
        given:
        String accountNumber = createAccount(new AccountCreateRequest('foo', 'bar', 'PL')).accountNumber
        Money money = new Money(10.111, PL.currency)

        when:
        ErrorResponse errorResponse = client.target(payInPath(accountNumber))
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(money))
                .readEntity(ErrorResponse.class)

        then:
        errorResponse.errors == ['amount numeric value out of bounds (<100 digits>.<2 digits> expected)']
    }

    def 'should transfer money between accounts'() {
        given:
        String sourceAccountNumber = createAccount(new AccountCreateRequest('foo', 'bar', 'PL')).accountNumber
        Money moneyToPayIn = new Money(150.00, PL.currency)
        payIn(sourceAccountNumber, moneyToPayIn)

        String destinationAccountNumber = createAccount(new AccountCreateRequest('foo', 'bar', 'PL')).accountNumber
        Money moneyToTransfer = new Money(100, PL.currency)
        Transfer transfer = new Transfer(destinationAccountNumber, moneyToTransfer)

        when:
        Response response = makeTransfer(sourceAccountNumber, transfer, Response.class)

        then:
        response.status == NO_CONTENT_204

        then:
        AccountResponse sourceAccount = getAccount(sourceAccountNumber)
        sourceAccount.balance == moneyToPayIn.amount.subtract(moneyToTransfer.amount)
        sourceAccount.operations.size() == 2
        sourceAccount.operations[1].operationType == TRANSFER
        sourceAccount.operations[1].amount == moneyToTransfer.amount.negate()

        and:
        AccountResponse destinationAccount = getAccount(destinationAccountNumber)
        destinationAccount.balance == moneyToTransfer.amount
        destinationAccount.operations.size() == 1
        destinationAccount.operations[0].operationType == TRANSFER
        destinationAccount.operations[0].amount == moneyToTransfer.amount
    }

    def 'should return BAD_REQUEST when not enough money to transfer from source account'() {
        given:
        String sourceAccountNumber = createAccount(new AccountCreateRequest('foo', 'bar', 'PL')).accountNumber
        String destinationAccountNumber = createAccount(new AccountCreateRequest('foo', 'bar', 'PL')).accountNumber
        Money moneyToTransfer = new Money(100, PL.currency)
        Transfer transfer = new Transfer(destinationAccountNumber, moneyToTransfer)

        when:
        ErrorResponse errorResponse = client.target(transferPath(sourceAccountNumber))
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(transfer))
                .readEntity(ErrorResponse.class)

        then:
        errorResponse.code == BAD_REQUEST_400
        errorResponse.message ==
                "Insufficient amount of money to make transfer (available=0, needed=$moneyToTransfer.amount)" as String
    }

    private AccountResponse createAccount(AccountCreateRequest accountCreateRequest) {
        client.target(ACCOUNTS_PATH)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(accountCreateRequest), AccountResponse)
    }

    private AccountResponse getAccount(String accountNumber) {
        client.target(ACCOUNTS_PATH)
                .queryParam('accountNumber', accountNumber)
                .request(MediaType.APPLICATION_JSON)
                .get(AccountResponse)
    }

    private Response payIn(String accountNumber, Money money) {
        client.target(payInPath(accountNumber))
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(money), Response.class)
    }

    private Response makeTransfer(String sourceAccountNumber, Transfer transfer, Class<Response> responseClass) {
        client.target(transferPath(sourceAccountNumber))
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(transfer), responseClass)
    }

    private static String payInPath(String accountNumber) {
        "$ACCOUNTS_PATH/$accountNumber/operations/pay-in"
    }

    private static String transferPath(String accountNumber) {
        "$ACCOUNTS_PATH/$accountNumber/operations/transfer"
    }
}
