package com.revolut.tech.resources.account
import com.revolut.tech.ErrorResponse
import com.revolut.tech.SpecificationIT
import com.revolut.tech.api.account.AccountCreateRequest
import com.revolut.tech.api.account.AccountResponse
import org.iban4j.Iban
import spock.lang.Unroll

import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType

import static java.math.BigDecimal.ZERO
import static org.eclipse.jetty.http.HttpStatus.NOT_FOUND_404
import static org.iban4j.IbanUtil.validate

class AccountResourceIT extends SpecificationIT {

    private static final String ACCOUNTS_PATH = 'http://localhost:8080/account'

    def 'should return created account'() {
        given:
        AccountCreateRequest accountCreateRequest = new AccountCreateRequest('foo', 'bar', 'PL')

        when:
        AccountResponse response = createAccount(accountCreateRequest)

        then:
        response.name == 'foo'
        response.surname == 'bar'
        response.balance == ZERO
        validate(response.accountNumber)
    }

    def 'should return account by account number'() {
        given:
        AccountCreateRequest accountCreateRequest = new AccountCreateRequest('foo', 'bar', 'PL')
        AccountResponse expectedAccount = createAccount(accountCreateRequest)

        when:
        AccountResponse actualAccount = getAccount(expectedAccount.accountNumber)

        then:
        actualAccount == expectedAccount
    }

    def 'should return NOT FOUND when account does not exist'() {
        given:
        String nonExistingAccountNumber = Iban.random().toString()

        when:
        ErrorResponse response = client.target(ACCOUNTS_PATH)
                .queryParam('accountNumber', nonExistingAccountNumber)
                .request(MediaType.APPLICATION_JSON)
                .get()
                .readEntity(ErrorResponse.class)

        then:
        response.code == NOT_FOUND_404
        response.message == "Account with number $nonExistingAccountNumber does not exist" as String
    }

    @Unroll
    def 'returns error when request #name is null or blank'() {
        when:
        ErrorResponse errorResponse = client.target(ACCOUNTS_PATH)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(request))
                .readEntity(ErrorResponse.class)

        then:
        errorResponse.errors == ["$field may not be empty" as String]

        where:
        request                                      | field
        new AccountCreateRequest(null, 'bar', 'PL')  | 'name'
        new AccountCreateRequest('foo', null, 'PL')  | 'surname'
        new AccountCreateRequest('foo', 'bar', null) | 'countryCode'
    }

    private AccountResponse createAccount(AccountCreateRequest accountCreateRequest) {
        client.target(ACCOUNTS_PATH)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(accountCreateRequest), AccountResponse);
    }

    private AccountResponse getAccount(String accountNumber) {
        client.target(ACCOUNTS_PATH)
                .queryParam('accountNumber', accountNumber)
                .request(MediaType.APPLICATION_JSON)
                .get(AccountResponse)
    }
}
